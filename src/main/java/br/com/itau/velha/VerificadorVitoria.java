package br.com.itau.velha;

public class VerificadorVitoria {

  private static boolean verificarHorizontal1(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    return casas[0][0] != Simbolo.VAZIO && casas[0][0] == casas[0][1] && casas[0][1] == casas[0][2];
  }
  
  private static boolean verificarHorizontal2(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    return casas[1][0] != Simbolo.VAZIO && casas[1][0] == casas[1][1] && casas[1][1] == casas[1][2];
  }
  
  private static boolean verificarHorizontal3(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    return casas[2][0] != Simbolo.VAZIO && casas[2][0] == casas[2][1] && casas[2][1] == casas[2][2];
  }
  
  private static boolean verificarVertical1(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    return casas[0][0] != Simbolo.VAZIO && casas[0][0] == casas[1][0] && casas[1][0] == casas[2][0];
  }
  
  private static boolean verificarVertical2(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    return casas[0][1] != Simbolo.VAZIO && casas[0][1] == casas[1][1] && casas[1][1] == casas[2][1];
  }
  
  private static boolean verificarVertical3(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    return casas[0][2] != Simbolo.VAZIO && casas[0][2] == casas[1][2] && casas[1][2] == casas[2][2];
  }
  
  private static boolean verificarDiagonal1(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    return casas[0][0] != Simbolo.VAZIO && casas[0][0] == casas[1][1] && casas[1][1] == casas[2][2];
  }
  
  private static boolean verificarDiagonal2(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    return casas[0][2] != Simbolo.VAZIO && casas[0][2] == casas[1][1] && casas[1][1] == casas[2][0];
  }
  
  public static boolean verificarVelha(Tabuleiro tabuleiro) {
    Simbolo[][] casas = tabuleiro.getCasas();
    
    for (int i = 0; i < casas.length; i++) {
      for (int j = 0; j < casas[i].length; j++) {
        if(Simbolo.VAZIO == casas[i][j]) {
          return false;
        }
      }
    }
    
    return true;
  }
  
  public static boolean verificar(Tabuleiro tabuleiro) {
    return ! verificarVelha(tabuleiro) && 
        verificarDiagonal1(tabuleiro) ||
        verificarDiagonal2(tabuleiro) ||
        verificarHorizontal1(tabuleiro) ||
        verificarHorizontal2(tabuleiro) ||
        verificarHorizontal3(tabuleiro) ||
        verificarVertical1(tabuleiro) ||
        verificarVertical2(tabuleiro) || 
        verificarVertical3(tabuleiro);
  }
  
}
