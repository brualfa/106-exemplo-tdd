package br.com.itau.velha;

import br.com.itau.velha.exceptions.CasaInexistente;
import br.com.itau.velha.exceptions.CasaPreenchida;
import br.com.itau.velha.exceptions.JogadaIlegal;

public class Tabuleiro {
  private Simbolo[][] casas;

  public Tabuleiro() {
    casas = new Simbolo[3][3];
    inicializar();
  }
  
  public void jogar(int x, int y, Simbolo simbolo) 
      throws CasaInexistente, CasaPreenchida, JogadaIlegal {
    if(simbolo == Simbolo.VAZIO) {
      throw new JogadaIlegal();
    }
    
    if(x > 2 || x < 0 || y > 2 || y < 0) {
      throw new CasaInexistente();
    }
    
    if(casas[x][y] != Simbolo.VAZIO) {
      throw new CasaPreenchida();
    }
    
    casas[x][y] = simbolo;
  }
  
  public Simbolo[][] getCasas() {
    return casas;
  }
  
  public void inicializar() {
    for(int i = 0; i < 3; i++) {
      for(int j = 0; j < 3; j++) {
        casas[i][j] = Simbolo.VAZIO;
      }
    }
  }
}
