package br.com.itau.velha;

import br.com.itau.velha.exceptions.CasaInexistente;
import br.com.itau.velha.exceptions.CasaPreenchida;
import br.com.itau.velha.exceptions.JogadaIlegal;

public class Jogo {
  Jogador[] jogadores;
  Tabuleiro tabuleiro;
  int jogadorAtivo;
  boolean estaEncerrado;
  boolean eVelha;
  
  public Jogo() {
    tabuleiro = new Tabuleiro();
    jogadores = new Jogador[2];
    
    jogadores[0] = new Jogador(Simbolo.X);
    jogadores[1] = new Jogador(Simbolo.O);
    
    jogadorAtivo = 0;
  }
  
  public void jogar(int x, int y) throws CasaInexistente, CasaPreenchida, JogadaIlegal {
    tabuleiro.jogar(x, y, jogadores[jogadorAtivo].getSimbolo());
    
    verificarVitoria();
    
    if(estaEncerrado) {
      return;
    }
    
    alternarJogador();
  }
  
  private void alternarJogador() {
    if(jogadorAtivo == 0) {
      jogadorAtivo = 1; 
    } else {
      jogadorAtivo = 0;
    }
  }

  private void verificarVitoria() {
    estaEncerrado = VerificadorVitoria.verificar(tabuleiro);
    eVelha = VerificadorVitoria.verificarVelha(tabuleiro);
  }

}
