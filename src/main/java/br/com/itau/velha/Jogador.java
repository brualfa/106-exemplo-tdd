package br.com.itau.velha;

public class Jogador {
  private Simbolo simbolo;
  
  public Jogador(Simbolo simbolo) {
    this.simbolo = simbolo;
  }
  
  public Simbolo getSimbolo() {
    return simbolo;
  }
}
