package br.com.itau.velha;

import org.junit.Test;

import br.com.itau.velha.exceptions.CasaInexistente;
import br.com.itau.velha.exceptions.CasaPreenchida;
import br.com.itau.velha.exceptions.JogadaIlegal;

import static org.junit.Assert.assertEquals;

import org.junit.Before;

public class TabuleiroTest {
  
  Tabuleiro tabuleiro;
  
  @Before
  public void preparar() {
    tabuleiro = new Tabuleiro();
  }

  @Test
  public void deveInicializarOTabuleiroComCasasVazias() {
    Simbolo[][] casas = tabuleiro.getCasas();
    
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        assertEquals(Simbolo.VAZIO, casas[i][j]);
      }
    }
  }
  
  @Test
  public void deveFazerUmaJogada() throws Exception {
    tabuleiro.jogar(0, 0, Simbolo.O);
    
    assertEquals(Simbolo.O,tabuleiro.getCasas()[0][0]);
  }
  
  @Test(expected = CasaInexistente.class)
  public void deveLancarExcessaoQuandoJogarEmCasaInexistente() throws Exception {
    tabuleiro.jogar(3, 0, Simbolo.O);
  }
  
  @Test(expected = CasaPreenchida.class)
  public void deveLancarExcessaoQuandoJogarEmCasaPreenchida() throws Exception {
    tabuleiro.jogar(0, 0, Simbolo.O);
    tabuleiro.jogar(0, 0, Simbolo.O);
  }
  
  @Test(expected = JogadaIlegal.class)
  public void deveLancarExcessaoQuandoJogarSimboloVazio() throws Exception {
    tabuleiro.jogar(0, 0, Simbolo.VAZIO);
  }
  
  @Test
  public void deveResetarOTabuleiroCorretamente() throws Exception {
    tabuleiro.jogar(0, 0, Simbolo.O);
    tabuleiro.jogar(0, 1, Simbolo.X);
    
    tabuleiro.inicializar();
    
    Simbolo[][] casas = tabuleiro.getCasas();
    
    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        assertEquals(Simbolo.VAZIO, casas[i][j]);
      }
    }
  }
}

