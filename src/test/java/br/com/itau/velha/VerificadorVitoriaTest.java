package br.com.itau.velha;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class VerificadorVitoriaTest {

  Tabuleiro tabuleiro;
  
  @Before
  public void preparar() {
    tabuleiro = new Tabuleiro();
  }
  
  @Test
  public void testarVitoriaHorizontal1() throws Exception{
    tabuleiro.jogar(0, 0, Simbolo.X);
    tabuleiro.jogar(0, 1, Simbolo.X);
    tabuleiro.jogar(0, 2, Simbolo.X);
    
    assertTrue(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void testarVitoriaHorizontal2() throws Exception{  
    tabuleiro.jogar(1, 0, Simbolo.X);
    tabuleiro.jogar(1, 1, Simbolo.X);
    tabuleiro.jogar(1, 2, Simbolo.X);
    
    assertTrue(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void testarVitoriaHorizontal3() throws Exception{
    tabuleiro.jogar(2, 0, Simbolo.X);
    tabuleiro.jogar(2, 1, Simbolo.X);
    tabuleiro.jogar(2, 2, Simbolo.X);
    
    assertTrue(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void testarVitoriaVertical1() throws Exception{
    tabuleiro.jogar(0, 0, Simbolo.X);
    tabuleiro.jogar(1, 0, Simbolo.X);
    tabuleiro.jogar(2, 0, Simbolo.X);
    
    assertTrue(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void testarVitoriaVertical2() throws Exception{ 
    tabuleiro.jogar(0, 1, Simbolo.X);
    tabuleiro.jogar(1, 1, Simbolo.X);
    tabuleiro.jogar(2, 1, Simbolo.X);
    
    assertTrue(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void testarVitoriaVertical3() throws Exception{
    tabuleiro.jogar(0, 2, Simbolo.X);
    tabuleiro.jogar(1, 2, Simbolo.X);
    tabuleiro.jogar(2, 2, Simbolo.X);
    
    assertTrue(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void testarVitoriaDiagonal1() throws Exception{
    tabuleiro.jogar(0, 0, Simbolo.X);
    tabuleiro.jogar(1, 1, Simbolo.X);
    tabuleiro.jogar(2, 2, Simbolo.X);
    
    assertTrue(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void testarVitoriaDiagonal2() throws Exception{ 
    tabuleiro.jogar(0, 2, Simbolo.X);
    tabuleiro.jogar(1, 1, Simbolo.X);
    tabuleiro.jogar(2, 0, Simbolo.X);
    
    assertTrue(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void naoDeveResultarVitoriaParaCasasVazias() throws Exception{
    assertFalse(VerificadorVitoria.verificar(tabuleiro));
  }
  
  @Test
  public void deveVerificarVelha() throws Exception{
    tabuleiro.jogar(0, 1, Simbolo.X);
    tabuleiro.jogar(0, 2, Simbolo.X);
    tabuleiro.jogar(1, 0, Simbolo.X);
    tabuleiro.jogar(1, 1, Simbolo.X);
    tabuleiro.jogar(1, 2, Simbolo.X);
    tabuleiro.jogar(2, 0, Simbolo.X);
    tabuleiro.jogar(2, 1, Simbolo.X);
    tabuleiro.jogar(2, 2, Simbolo.X);
  }

}
