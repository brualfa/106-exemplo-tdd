package br.com.itau.velha;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;

public class JogoTest {

  private Jogo jogo;
  
  @Before
  public void preparar() {
    jogo = new Jogo();
  }
  
  @Test
  public void deveInicializarOJogoComTabuleiroEJogadores() {    
    assertNotNull(jogo.tabuleiro);
    assertNotNull(jogo.jogadores[0]);
    assertEquals(Simbolo.X, jogo.jogadores[0].getSimbolo());
    assertNotNull(jogo.jogadores[1]);
    assertEquals(Simbolo.O, jogo.jogadores[1].getSimbolo());
    assertEquals(0, jogo.jogadorAtivo);
    assertEquals(false, jogo.estaEncerrado);
    assertEquals(false, jogo.eVelha);
  }
  
  @Test
  public void deveJogarCorretamente() throws Exception {
    jogo.jogar(0, 0);
    
    assertEquals(Simbolo.X, jogo.tabuleiro.getCasas()[0][0]);
  }
  
  @Test
  public void deveAlternarEntreOsJogadores() throws Exception{
    jogo.jogar(0, 0);
    jogo.jogar(1, 0);
    
    assertEquals(Simbolo.X, jogo.tabuleiro.getCasas()[0][0]);
    assertEquals(Simbolo.O, jogo.tabuleiro.getCasas()[1][0]);
  }
  
  @Test
  public void deveVerificarVitoria() throws Exception{
    jogo.jogar(0, 0);
    jogo.jogar(0, 1);
    jogo.jogar(1, 0);
    jogo.jogar(1, 1);
    jogo.jogar(2, 0);
    
    assertTrue(jogo.estaEncerrado);
  }

}




